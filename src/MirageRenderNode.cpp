#include <ctime>

#include "MirageRenderNode.h"

#include <filter/NLM.h>
#include <utils/Timer.h>

using namespace DD::Image;

static const char* const renderTypeNames[] = {
    "CPU",
    "GPU (CUDA)",
    0
};

static const char* const filterTypeNames[] = {
    "Box",
    "Gaussian",
    0
};

static const Mirage::FilterType filterTypes[] = {
    Mirage::eFilterBox,
    Mirage::eFilterGaussian
};

static const char* const renderModeNames[] = {
    "Normals",
    "Complexity",
    "Path Trace",
    0
};

static const Mirage::RenderMode renderModes[] = {
    Mirage::eNormals,
    Mirage::eComplexity,
    Mirage::ePathTrace
};

MirageRender::MirageRender(Node* node) : Render(node, false/*enable_mip_filter*/)
{
    m_imageHeight = 512;
    m_imageWidth = 512;

    m_renderType = 0; // CPU
    m_filterType = 1; // Gaussian
    m_renderMode = 2; // Path Trace
    m_exposure = 1.0f;
    m_limit = 1.5;
    m_numSamples = 3;
    m_enableDOF = false;
    m_nlmWidth = 0;
    m_nlmFalloff = 200.0f;
    m_pRenderer = nullptr;
    m_pPixels = nullptr;
    m_pPixelsFiltered = nullptr;
}

void MirageRender::preRender()
{
    // build scene BHV
    m_scene.Build();

    // create color buffers
    m_pPixels = new Mirage::Color[m_renderOptions.width * m_renderOptions.height];
    m_pPixelsFiltered = new Mirage::Color[m_renderOptions.width * m_renderOptions.height];

    // initialize render
    m_pRenderer->Init(m_renderOptions.width, m_renderOptions.height);
}

void MirageRender::initRender()
{
    switch (m_renderType)
    {
        case 0: // CPU
        {
            m_pRenderer = Mirage::CreateCpuRenderer(&m_scene);
            return;
        }
        case 1: // GPU
        {
#if _WIN32
            // setup CUDA device
            const int device = 0;
            cudaSetDevice(device);
        
            cudaDeviceProp props;
            cudaError err = cudaGetDeviceProperties(&props, device);
            
            // require SM .0 GPU
            if (props.major < 5)
            {
                error("Need a compute capable 5.0 device\n");
                return;
            }

            // retrieve device name
            char name[256];
            memcpy((void*)name, props.name, 256);

            printf("Compute device: %s\n", name);

            m_pRenderer = Mirage::CreateGpuRenderer(&m_scene);
            return;
#else
            error("GPU render not implemented on this platform.");
            return;
#endif
        }
    }
}

void MirageRender::render()
{
    Mirage::Timer timer;
    for (auto i = 0; i < m_numSamples; i++)
    {
        std::cout << "Rendering pass " << i << std::endl;
        m_pRenderer->Render(m_Camera, m_renderOptions, m_pPixels);
    }
    std::cout << "Render took " << timer.elapsed() << " milliseconds" << std::endl;

    // map linear to sRGB
    if (m_renderMode == Mirage::ePathTrace)
    {
        int numPixels = m_imageWidth * m_imageHeight;
        for (int i=0; i < numPixels; ++i)
        {            
            float s = m_exposure / m_pPixels[i].w;
            m_pPixels[i] = Mirage::LinearToSrgb(Mirage::ToneMap(m_pPixels[i] * s, m_limit));
        }
    }
}

void MirageRender::postRender()
{
    // NLM filtering
    if (m_renderMode == Mirage::ePathTrace)
    {
        if (m_nlmWidth)
        {
            Mirage::NonLocalMeansFilter(m_pPixels, m_pPixelsFiltered, m_imageWidth, m_imageHeight, m_nlmFalloff, m_nlmWidth);
            m_pPixels = m_pPixelsFiltered;
        }
    }
}

void MirageRender::knobs(Knob_Callback f)
{
    // Render type (0 = CPU, 1 = GPU)
    Enumeration_knob(f, &m_renderType, renderTypeNames, "renderType");

    // Render mode
    Enumeration_knob(f, &m_renderMode, renderModeNames, "renderMode");

    // Filter
    Enumeration_knob(f, &m_filterType, filterTypeNames, "filterType");

    // Exposure
    Float_knob(f, &m_exposure, "exposure");
    
    // Limit
    Float_knob(f, &m_limit, "limit");

    // Samples
    Int_knob(f, &m_numSamples, "numSamples");

    // NLM filtering
    Int_knob(f, &m_nlmWidth, "nlmWidth");
    Float_knob(f, &m_nlmFalloff, "nlmFalloff");

    // DOF
    Bool_knob(f, &m_enableDOF, "enableDOF");
}

int MirageRender::knob_changed(Knob* k)
{
    if (k->is("renderType"))
    {
        initRender();
    }
    if (k->is("nlmWidth") || k->is("nlmFalloff"))
    {
        postRender();
    }
    return Iop::knob_changed(k);
}

CameraOp* MirageRender::getInputCameraOpForSampleAndView(uint32_t sample, int32_t  view)
{
    const int input_num = sample + view;

    Op* op = Op::input(2, input_num);
    CameraOp* cam = dynamic_cast<CameraOp*>(op);
    if (cam)
        return cam;

    return DD::Image::CameraOp::default_camera();
}

GeoOp* MirageRender::getInputGeoOpForSample(uint32_t sample)
{
    // objects start at input 1
    return dynamic_cast<GeoOp*>(Op::input(1 + sample));
}

Matrix4 MirageRender::camera_matrix(int sample)
{
    // CameraOp* cam = rtx.input_scenes[sample]->camera;
    // if (cam)
    // {
    //     cam->validate(true);
    //     return cam->imatrix();
    // }
    Matrix4 m;
    m.makeIdentity();

    return m;
}

Matrix4 MirageRender::projection_matrix(int sample)
{
    const Format& f = info_.format();
    const float W = float(f.width());
    const float H = float(f.height());

    // Determine aperture expansion due to a format with a defined inner image area.
    // Offset and scale the aperture:
    Matrix4 m;
    m.translation((float(f.r() + f.x()) / W) - 1.0f, (float(f.t() + f.y()) / H) - 1.0f);
    m.scale(float(f.w())/W, float(f.w())*float(f.pixel_aspect())/H, 1.0f);

    // TODO: we can't handle non-linear projections in here...
    // Apply linear projection:
    // CameraOp* cam = rtx.input_scenes[sample]->camera;
    // if (cam)
    // {
    //     cam->validate(true);
    //     m *= cam->projection(DD::Image::CameraOp::LENS_PERSPECTIVE);
    // }
    // else
    {
        Matrix4 p;
        p.projection(1.0f, 0.1f, 10000.0f);
        m *= p;
    }

    return m;
}

bool MirageRender::generate_render_primitives()
{
    preRender();
    render();
    postRender();
}

bool MirageRender::test_input(int input, Op* op) const
{
    switch (input) {
        case 0: return (dynamic_cast<Iop*>(op)      != NULL);
        case 1: return (dynamic_cast<GeoOp*>(op)    != NULL);
        case 2: return (dynamic_cast<CameraOp*>(op) != NULL);
    }
    return false;
}

DD::Image::Op* MirageRender::default_input(int input) const
{
    switch (input) {
        case 0: return Iop::default_input(input);
        case 1: return NULL; // GeoOp
        case 2: return NULL; // CameraOp::default_camera() might work
    }
    return NULL;
}

const char* MirageRender::input_label(int input, char* buffer) const
{
    switch (input) {
        case 0: return "bg";
        case 1: return "obj/scn";
        case 2: return "cam";
    }
    return buffer;
}

int MirageRender::split_input(int input) const
{
    if (input == 0)
    {
        // BG input - No multisampling needed:
        return 1;
    }
    else if (input == 1)
    {
        // GEO input - Geometry only needs splitting by number of samples:
        return this->samples();
    }
    else if (input == 2)
    {
        // CAMERA input - TODO: implement samples*views
        return 1;
    }
    return 1;
}

const OutputContext& MirageRender::inputContext(int input, int offset, OutputContext& context) const
{
    // Copy the context contents from this Op:
    context = outputContext();

    // No multisampling for bg input:
    if (input == 0)
        return context;

    // Geometry inputs are offset in time by sample count:
    if (input == 1)
    {
        context.setFrame(context.frame());
        //context.setView(rtx.k_hero_view);
        return context;
    }

    // Camera needs views as well:
    if (input == 2)
    {
        // Offset camera in time:
        context.setFrame(context.frame());

        // Get view for this offset:
        context.setView(context.view());

        return context;
    }

    return context;
}

Op::HandlesMode MirageRender::doAnyHandles(ViewerContext* ctx)
{
    //std::cout << "zpRender::doAnyHandles(): transform_mode=" << ctx->transform_mode() << std::endl;
    // Fix from Foundry to stop build_handles() from crashing validate():
    DD::Image::Op::HandlesMode need_handles = DD::Image::Iop::doAnyHandles(ctx);
    if (need_handles != DD::Image::Op::eNoHandles)
        need_handles = DD::Image::Op::eHandlesCooked;

    if (ctx->transform_mode() == DD::Image::VIEWER_2D &&
        ((ctx->connected() == DD::Image::SHOW_OBJECT && panel_visible()) ||
         (ctx->connected() == DD::Image::SHOW_PUSHED_OBJECT && pushed())))
    {
        return need_handles;
    }
    // Also need handles if panel is open:
    // TODO: Can we detect if we're inside a Gizmo...?
    if (panel_visible())
        return need_handles;

    return DD::Image::Iop::doAnyHandles(ctx);
}

void MirageRender::build_handles(DD::Image::ViewerContext* ctx)
{
    //std::cout << "zpRender::build_handles(): viewer_mode=" << ctx->viewer_mode() << ", transform_mode=" << ctx->transform_mode() << std::endl;
    const DD::Image::Matrix4 saved_matrix = ctx->modelmatrix;
    const int                saved_mode   = ctx->transform_mode();

    // Viewer appears to call the renderer with VIEWER_PERSP transform mode now,
    // not really sure why.
    // Check how other nodes call it too:
    if (ctx->viewer_mode()    == DD::Image::VIEWER_2D &&
        ctx->transform_mode() == DD::Image::VIEWER_PERSP)
    {
        DD::Image::Render::validate(false);

        ctx->addCamera(getInputCameraOpForSampleAndView(0/*sample*/, outputContext().view()));

        // Apply the renderer's formatting:
        // Scale and translate from NDC to format:
        ctx->modelmatrix *= get_format_matrix(0.0f/*dx*/, 0.0f/*dy*/);
        ctx->modelmatrix *= projection_matrix(0/*sample*/);
        ctx->modelmatrix *= camera_matrix(0/*sample*/);
        ctx->transform_mode(DD::Image::VIEWER_PERSP);
    }


    build_input_handles(ctx);

    // Restore transform mode and matrix:
    ctx->transform_mode(saved_mode);
    ctx->modelmatrix = saved_matrix;

    // Let local zpRender knobs draw:
    build_knob_handles(ctx);
}

void MirageRender::append(Hash& hash)
{
    const int view0 = 1;
    DD::Image::CameraOp* cam = getInputCameraOpForSampleAndView(0, view0);
    if (cam)
        hash.append(cam->hash());

    DD::Image::Render::append(hash);
}

void MirageRender::_validate(bool for_real)
{
    copy_info();

    m_imageWidth = info_.w();
    m_imageHeight = info_.h();

    m_renderOptions.width = m_imageWidth;
    m_renderOptions.height = m_imageHeight;
    m_renderOptions.filter = Mirage::Filter(filterTypes[m_filterType], 0.75f, 1.0f);
    m_renderOptions.mode = renderModes[m_renderMode];
    m_renderOptions.exposure = m_exposure;
    m_renderOptions.limit = m_limit;
	m_renderOptions.clamp = FLT_MAX;
	m_renderOptions.maxDepth = 4;
	m_renderOptions.maxSamples = m_numSamples; // this controls amount of motion blur
    m_renderOptions.enableDOF = m_enableDOF;

    initRender();

    m_scene.Clear();

    // build the render state hash
    DD::Image::Hash new_hash;
    {
        // Make hash unique to this Render op instance:
        Op* render_op = DD::Image::Op::firstOp();
        new_hash.append(&render_op, sizeof(void*));
    }
    Render::format().append(new_hash);

    m_needsRender = buildScene();
}

void MirageRender::_request(int x, int y, int r, int t, ChannelMask output_channels, int count)
{
    // These are the channels we get from our background input:
    DD::Image::ChannelSet bg_get_channels(output_channels);

    // Request the background image source:
    DD::Image::ChannelSet request_channels(bg_get_channels);
    request_channels += input0().channels();
    input0().request(x, y, r, t, request_channels, count);
}

void MirageRender::_close()
{
    // TODO: clear scene?
    m_scene.Clear();
}

void MirageRender::initialize()
{
    if (m_needsRender)
    {
        m_lock.lock();
        preRender();
        render();
        postRender();
        m_lock.unlock();
        m_needsRender = false;
    }
}

void MirageRender::engine(int y, int x, int r, ChannelMask channels, Row& row)
{
    initialize();
    
    if (m_pPixels != nullptr)
    {
        // copy buffer
        Mirage::Color* pImage = m_pPixels;

        // flip image vertically
        y = m_imageHeight - 1 - y; 

        foreach(z, channels)
        {
            float* outptr = row.writable(z) + x;

            for (int cur = x; cur < r; cur++)
            {
                int offset = (y * m_imageWidth + cur) * 4;
                *outptr++ = (*pImage)[offset + z - 1];
            }
        }
    }
    else
    {
        // fill buffer with grey color
        foreach(z, channels)
        {
            float* outptr = row.writable(z);
            for (int cur = x ; cur < r ; cur++)
            {
                outptr[cur] = 0.18f;
            }
        }
    }
}

static Op* build(Node* node) { return new MirageRender(node); }
const Op::Description MirageRender::description(CLASS, "MirageRender", build);
