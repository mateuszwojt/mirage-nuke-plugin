#include "DisneyShaderNode.h"

using namespace DD::Image;

// static const Mirage::BSDFType bsdfTypes[] = {
//     Mirage::eReflected,
//     Mirage::eTransmitted,
//     Mirage::eSpecular
// };

static const char* const bsdfTypeNames[] = {
    "Reflected",
    "Transmitted",
    "Specular",
    0
};

DisneyShader::DisneyShader(Node* node) : IllumShader(node)
{
    // initialize shader param values
    m_emission.set(0.0f, 0.0f, 0.0f);
    m_color.set(0.5f, 0.5f, 0.5f);
    m_absorption.set(0.0f, 0.0f, 0.0f);

    m_bsdfType = 2; // Specular BSDF

    m_eta = 0.0f;
    m_metallic = 0.0f;
    m_subsurface = 0.0f;
    m_specular = 0.5f;
    m_roughness = 0.5f;
    m_specularTint = 0.0f;
    m_anisotropic = 0.0f;
    m_sheen = 0.0f;
    m_sheenTint = 0.0f;
    m_clearcoat = 0.0f;
    m_clearcoatGloss = 1.0f;
    m_transmission = 0.0f;
}

void DisneyShader::exportMaterial(Mirage::Material* material)
{
    material->emission = Mirage::Vec3(m_emission.x, m_emission.y, m_emission.z);
    material->color = Mirage::Vec3(m_color.x, m_color.y, m_color.z);
    material->absorption = Mirage::Vec3(m_absorption.x, m_absorption.y, m_absorption.z);

    material->eta = m_eta;
    material->metallic = m_metallic;
    material->subsurface = m_subsurface;
    material->specular = m_specular;
    material->roughness = m_roughness;
    material->specularTint = m_specularTint;
    material->anisotropic = m_anisotropic;
    material->sheen = m_sheen;
    material->sheenTint = m_sheenTint;
    material->clearcoat = m_clearcoat;
    material->clearcoatGloss = m_clearcoatGloss;
    material->transmission = m_transmission;
}

Op* DisneyShader::default_input(int input) const
{
    if (input == 0)
    {
        return Material::default_input(input);
    }
    return 0;
}

const char* DisneyShader::input_label(int input, char* buffer) const
{
    switch (input) {
        default: return "";
        case 1: return "mapD";
        case 2: return "mapE";
        case 3: return "mapS";
        case 4: return "mapSh";
    }
}

void DisneyShader::_validate(bool for_real)
{
    Material::_validate(for_real);

    // Build surface ChannelMask from channel selector:
    surface_channels = Mask_None;
    for (int i = 0; i < 4; i++)
      surface_channels += channel[i];

    info_.turn_on(surface_channels);

    // Validate the image input:
    if (input(1))
        input1().validate(for_real);
    if (input(2))
        input(2)->validate(for_real);
    if (input(3))
        input(3)->validate(for_real);
    if (input(4))
        input(4)->validate(for_real);
}

void DisneyShader::_request(int x, int y, int r, int t, ChannelMask channels, int count)
{
    ChannelSet c1(channels);
    c1 += surface_channels;
    Material::_request(x, y, r, t, c1, count);

    // Request RGB from map input
    if (input(1)) 
    {
      const Box& b = input1().info();
      input1().request(b.x(), b.y(), b.r(), b.t(), Mask_RGB, count);
    }
    if (input(2)) 
    {
      const Box& b = input(2)->info();
      input(2)->request(b.x(), b.y(), b.r(), b.t(), Mask_RGB, count);
    }
    if (input(3)) 
    {
      const Box& b = input(3)->info();
      input(3)->request(b.x(), b.y(), b.r(), b.t(), Mask_RGB, count);
    }
    if (input(4)) 
    {
      const Box& b = input(4)->info();
      input(4)->request(b.x(), b.y(), b.r(), b.t(), Mask_RGBA, count);
    }
}

void DisneyShader::knobs(Knob_Callback f)
{
    IllumShader::knobs(f);

    Enumeration_knob(f, &m_bsdfType, bsdfTypeNames, "bsdfType");

    Color_knob(f, &m_emission.x, IRange(0, 4), "emission");
    Color_knob(f, &m_color.x, IRange(0, 4), "color");
    Color_knob(f, &m_absorption.x, IRange(0, 4), "absorption");

    Float_knob(f, &m_eta, "eta");
    Float_knob(f, &m_metallic, "metallic");
    Float_knob(f, &m_subsurface, "subsurface");
    Float_knob(f, &m_specular, "specular");
    Float_knob(f, &m_roughness, "roughness");
    Float_knob(f, &m_specularTint, "specularTint");
    Float_knob(f, &m_anisotropic, "anisotropic");
    Float_knob(f, &m_sheen, "sheen");
    Float_knob(f, &m_sheenTint, "sheenTint");
    Float_knob(f, &m_clearcoat, "clearcoat");
    Float_knob(f, &m_clearcoatGloss, "clearcoatGloss");
    Float_knob(f, &m_transmission, "transmission");
}

void DisneyShader::surface_shader(Vector3& P, Vector3& V, Vector3& N, const VertexContext& vtx, Pixel& surface)
{

}

bool DisneyShader::shade_GL(ViewerContext* ctx, GeoInfo& geo)
{
    // Let input set itself up first
    input0().shade_GL(ctx, geo);
    // do input 1 if input 0 is not connected
    if (input(1) != 0 && input(0) == default_input(0))
        input1().shade_GL(ctx, geo);

    // Don't bother with any custom shading:
    if (ctx->lights().size() == 0)
        return true;

    
    // Enable opacity
    glEnable(GL_BLEND);

    // Specify the default material type:
    Vector4 tmp;
    tmp.set(m_color, 1);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,  (GLfloat*)tmp.array());

    tmp.set(m_specular * m_specularTint, 1);
    glMaterialfv(GL_FRONT, GL_SPECULAR, (GLfloat*)tmp.array());

    tmp.set(m_emission, 1);
    glMaterialfv(GL_FRONT, GL_EMISSION, (GLfloat*)tmp.array());

    glMaterialf(GL_FRONT, GL_SHININESS, m_metallic * 10);

    glGetErrors("Disney shader");

    return true;
}

bool DisneyShader::set_texturemap(ViewerContext* ctx, bool gl)
{
    // use input 1 if input 0 is not connected
    if (input(1) != 0 && input(0) == default_input(0))
    {
        return input1().set_texturemap(ctx, gl);
    }
    else
    {
        return input0().set_texturemap(ctx, gl);
    }
}

static Op* build(Node* node)
{
    return new DisneyShader(node);
}

const Op::Description DisneyShader::description("DisneyShader", "DisneyShader", build);