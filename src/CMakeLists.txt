# set libraries list for linking
if(WINDOWS)
  set(ALL_LIBS
    opengl32
    legacy_stdio_definitions
  )
else()
  set(ALL_LIBS
    ${OPENGL_LIBRARIES}
  )
endif()

# build plugin
add_library(MirageNuke SHARED MirageRenderNode.cpp SceneProcessing.cpp DisneyShaderNode.cpp ${CU_FILES})
if(WIN32)
  set_target_properties(MirageNuke PROPERTIES CUDA_SEPARABLE_COMPILATION ON)
endif()

set_target_properties(MirageNuke PROPERTIES PREFIX "")
target_include_directories(
  MirageNuke
  PRIVATE
  ${CMAKE_SOURCE_DIR}/include
  ${CMAKE_SOURCE_DIR}/mirage/include
  ${Nuke_INCLUDE_DIR}
)
target_link_libraries(MirageNuke PUBLIC ${ALL_LIBS} ${Nuke_LIBRARIES} Mirage)
if(WIN32)
  target_link_libraries(MirageNuke ${CUDA_LIBRARIES})
endif()