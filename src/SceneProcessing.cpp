#include "MirageRenderNode.h"

#include "DDImage/CameraOp.h"
#include "DDImage/LightOp.h"
#include "DDImage/GeoOp.h"
#include "DDImage/Format.h"
#include "DDImage/Scene.h"
#include "DDImage/ImagePlane.h"
#include "DDImage/Quaternion.h"

#include <utils/Util.h>

using namespace DD::Image;

void MirageRender::translateCamera(CameraOp* pCamera)
{
    std::cout << "Start: Translating camera: " << pCamera->node_name() << std::endl;

    // get Nuke Camera position and rotation values
    Knob* pTrans = pCamera->knob("translate");
    Knob* pRotate = pCamera->knob("rotate");

    if (pTrans == nullptr || pRotate == nullptr)
    {
        std::cout << "Camera knobs are nullptr, returning." << std::endl;
        return;
    }
    
    Vector3 position(pTrans->get_value(0), pTrans->get_value(1), pTrans->get_value(2));
    Vector3 rotation(pRotate->get_value(0), pRotate->get_value(1), pRotate->get_value(2));

    // set camera position (inverse Y to match transforms)
    m_Camera.position = Mirage::Vec3(position.x, position.y, position.z);
    std::cout << "\tCamera position : " << position.x << ", " << position.y << ", " << position.z << std::endl;

    // FIXME: rotation in Y axis needs to be flipped
    Matrix4 rotMat = pCamera->matrix();
    rotMat.rotationOnly();
    Mirage::Mat44 tM = Mirage::Mat44(rotMat.a00, rotMat.a01, rotMat.a02, rotMat.a03,
                                rotMat.a10, rotMat.a11, rotMat.a12, rotMat.a13,
                                rotMat.a20, rotMat.a21, rotMat.a22, rotMat.a23,
                                rotMat.a30, rotMat.a31, rotMat.a32, rotMat.a33);
    Mirage::Mat33 m(Mirage::Vec3(tM.GetCol(0)), Mirage::Vec3(tM.GetCol(1)), Mirage::Vec3(tM.GetCol(2)));
    m_Camera.rotation = Mirage::Quat(m);
    std::cout << "\tCamera rotation : " << rotation.x << ", " << rotation.y << ", " << rotation.z << std::endl;
    
    // calculate Field of View
    float focalLength = pCamera->focal_length();
    float horizAperture = pCamera->film_width();
    float fov = 2.0f * std::atan((horizAperture / 2.0f) / focalLength);
    m_Camera.fov = fov;
    std::cout << "\tCamera FOV : " << m_Camera.fov << std::endl;

    // get focal point and fStop
    m_Camera.focalPoint = pCamera->focal_point();
    m_Camera.aperture = pCamera->fstop();
    std::cout << "\tCamera focal point : " << m_Camera.focalPoint << std::endl;
    std::cout << "\tCamera aperture : " << m_Camera.aperture << std::endl;
}

void MirageRender::translateLight(LightOp* pLight)
{
    std::cout << "Start: Translating light: " << pLight->node_name() << std::endl;

    Knob* pTrans = pLight->knob("translate");
    Mirage::Vec3 position(pTrans->get_value(0), pTrans->get_value(1), pTrans->get_value(2));

    Knob* pRotate = pLight->knob("rotate");
    Mirage::Vec3 rotate(pRotate->get_value(0), pRotate->get_value(1), pRotate->get_value(2));

    // Knob* pScale = pLight->knob("scaling");
    Knob* pUniformScale = pLight->knob("uniform_scale");
    float scale = pUniformScale->get_value(0);

    Pixel lightColor = pLight->color();
    float* pColor = lightColor.array();
    Mirage::Vec3 color(pColor[1], pColor[2], pColor[3]);
    std::cout << "\tLight color : " << pColor[1] << ", " << pColor[2] << ", " << pColor[3] <<  std::endl;

    float intensity = pLight->intensity();
    std::cout << "\tLight intensity : " << intensity << std::endl;

    switch (pLight->lightType())
    {
        case LightOp::ePointLight:
        {
            // create new light instance
            Mirage::Light light;
            light.sphere.radius = scale * 0.1f; // scale it down, default sphere is quite huge
            light.lightSamples = 1;
            // transforms
            light.position = position;
            light.startTransform.p = position;
            light.endTransform.p = position;
            // TODO: add rotation (how to handle area lights?)

            Mirage::Material lightMat;
            // FIXME: calculate emission in more physically-based fashion
            lightMat.emission = color * intensity * 1000.0f;
            light.material = lightMat;

            m_scene.AddPrimitive(light);
        }
        case LightOp::eOtherLight: // Environment light
        {
            // create new sky instance
            Mirage::Sky sky;
            Mirage::Probe probe;

            sky.horizon = color * intensity;
            sky.zenith = 0.5f + rotate * 0.1f;
            sky.probe = probe;
            m_scene.sky = sky;
            return;
        }
        default: { break; }
    }
}

void MirageRender::translateGeo(DD::Image::GeoInfo& geoInfo)
{
    GeoOp* pTmpGeo = geoInfo.source_geo;
    std::string meshName = pTmpGeo->node_name();
    std::cout << "Start: Translating geo: " << meshName << std::endl;

    Mirage::Mesh* mesh = new Mirage::Mesh();
    Mirage::Primitive primitive;
    primitive.type = Mirage::eMesh;

    // transform primitive
    Knob* translate = pTmpGeo->knob("translate");
    primitive.startTransform.p = Mirage::Vec3(translate->get_value(0), translate->get_value(1), translate->get_value(2));
    primitive.endTransform.p = primitive.startTransform.p; // no animation temporarily... sad panda

    // rotate primitive
    Matrix4 rotation = geoInfo.matrix;
    rotation.rotationOnly();
    Mirage::Mat44 r = Mirage::Mat44(rotation.a00, rotation.a01, rotation.a02, rotation.a03,
                rotation.a10, rotation.a11, rotation.a12, rotation.a13,
                rotation.a20, rotation.a21, rotation.a22, rotation.a23,
                rotation.a30, rotation.a31, rotation.a32, rotation.a33);
    Mirage::Mat33 m(Mirage::Vec3(r.GetCol(0)), Mirage::Vec3(r.GetCol(1)), Mirage::Vec3(r.GetCol(2)));
    primitive.startTransform.r = Mirage::Quat(m);
    primitive.endTransform.r = primitive.startTransform.r; // no animation temporarily... sad panda

    // scale primitive
    // Knob* scale = pTmpGeo->knob("scaling"); 
    Knob* uniformScale = pTmpGeo->knob("uniform_scale"); // FIXME: Mirage only accepts single float as scale value
    primitive.startTransform.s = uniformScale->get_value(0);
    primitive.endTransform.s = primitive.startTransform.s;

    // get num of prims and points to reserve some memory
    unsigned int numPrims = geoInfo.primitives();
    // unsigned int* pFaceVertices;

    // iterate over primitives
    for (unsigned int primIndex = 0; primIndex < numPrims; primIndex++)
    {
        const Primitive* prim = geoInfo.primitive(primIndex);
        unsigned int numVertices = prim->vertices();
        
        // TODO: find a way to process polygons as triangles
        for (unsigned int verticeIndex = 0; verticeIndex < numVertices; verticeIndex++)
        {
            mesh->indices.emplace_back(prim->vertex(verticeIndex));
        }
    }

    // Points
    const PointList* geoPoints = geoInfo.point_list();
    int numPoints = geoPoints->size();

    for (int i=0; i < numPoints; i++)
    {
        // get point position in world space  
        const Vector3& localPoint = geoInfo.point_array()[i];
        mesh->vertices.emplace_back(Mirage::Vec3(localPoint.x, localPoint.y, localPoint.z));
    }

    // UVs
    bool haveUVs = false;
    const Attribute* uvs = geoInfo.get_typed_group_attribute(Group_Points, kUVAttrName, VECTOR4_ATTRIB);
    if (!uvs)
    {
        uvs = geoInfo.get_typed_group_attribute(Group_Vertices, kUVAttrName, VECTOR4_ATTRIB);
    }

    if (uvs)
    {
        haveUVs = true;
        for (size_t i = 0; i < uvs->size(); i++)
        {
            Vector4 uv = uvs->vector4(i);
            Mirage::Vec2 newUV(uv.x, uv.y);
            mesh->uvs.emplace_back(newUV);
        }
    }

    // Normals
    const Attribute* normals = geoInfo.get_typed_group_attribute(Group_Points, kNormalAttrName, NORMAL_ATTRIB);
    if (!normals)
    {
        normals = geoInfo.get_typed_group_attribute(Group_Vertices, kNormalAttrName, NORMAL_ATTRIB);
    }

    if (normals)
    {
        for (size_t i = 0; i < normals->size(); i++)
        {
            const Vector3& n = normals->normal();
            Mirage::Vec3 newNormal(n.x, n.y, n.z);
            mesh->normals.emplace_back(newNormal);
        }
    }
    else
    {
        // recalculate normals if provided geo has none 
        mesh->CalculateNormals();
    }

    // rebuild acceleration structure
    mesh->rebuildBVH();

    // print more stats
    std::cout << "\tVertices : " << mesh->vertices.size() << std::endl;
    std::cout << "\tNormals : " << mesh->normals.size() << std::endl;
    std::cout << "\tUVs : " << mesh->uvs.size() << std::endl;
    std::cout << "\tIndices : " << mesh->indices.size() << std::endl;
    std::cout << "\tCDF : " << mesh->cdf.size() << std::endl;
    
    primitive.mesh = Mirage::GeometryFromMesh(mesh);

    if (geoInfo.material)
    {
        Mirage::Material material;
        translateMaterial(&material, geoInfo.material, haveUVs);
        primitive.material = material;
    }

    m_scene.AddPrimitive(primitive);
    m_scene.AddMesh(mesh);
}

void MirageRender::translateMaterial(Mirage::Material* pMaterial, Iop* pNukeMaterial, bool haveUVs)
{
    std::cout << "Start: Translating pMaterial: " << pNukeMaterial->node_name() << std::endl;

    if (strcmp(pNukeMaterial->Class(), "DisneyShader") == 0)
    {
        std::cout << "Disney Shader detected" << std::endl;
        DisneyShader* shader = reinterpret_cast<DisneyShader*>(pNukeMaterial);
        shader->exportMaterial(pMaterial);
        return;
    }

    pNukeMaterial->request(Mask_RGBA, 1);

    // see if we've got a default pMaterial
    float red = pNukeMaterial->at(0, 0, Chan_Red);
    float green = pNukeMaterial->at(0, 0, Chan_Green);
    float blue = pNukeMaterial->at(0, 0, Chan_Blue);
    // float alpha = pNukeMaterial->at(0, 0, Chan_Alpha);

    if (haveUVs)
    {
        // pMaterial has UVs, try to obtain the color or the texture
        unsigned int width = pNukeMaterial->info().w();
        unsigned int height = pNukeMaterial->info().h();

        bool isConstant = (width == 1 && height == 1);
        bool haveAlpha = pNukeMaterial->info().channels().contains(Chan_Alpha);

        // fetch texture from image plane
        Box bbox(0, 0, width, height);
        ImagePlane newImagePlane(bbox, true, haveAlpha ? Mask_RGBA : Mask_RGB, haveAlpha ? 4 : 3);
        // make a texture value pointer
        pNukeMaterial->fetchPlane(newImagePlane);
        const float* pRawValues = newImagePlane.readable();

        if (isConstant)
        {
            std::cout << "\tConstant color detected" << std::endl;

            float red = *pRawValues++;
            float green = *pRawValues++;
            float blue = *pRawValues++;

            if (red == 0.0f && green == 0.0f && blue == 0.0f)
            {
                // detect black - set it to dark grey so we can see something...
                // to match behavior of the ScanlineRender it should be left black
                pMaterial->color = Mirage::Vec3(0.4f, 0.4f, 0.4f);
            }
            else
            {
                pMaterial->color = Mirage::Vec3(red, green, blue);
            }
        }
        else
        {
            // material has texture
            std::cout << "\tTexture detected" << std::endl;
            // create texture
            Mirage::Texture tex;
            unsigned int texSize = width * height * 3;
            std::vector<float> tmpData;
            tmpData.resize(texSize);

            for (unsigned int y = 0; y < height; y++)
            {
                for (unsigned int x = 0; x < width; x++)
                {
                    float red = *pRawValues++;
                    float green = *pRawValues++;
                    float blue = *pRawValues++;

                    if (haveAlpha)
                    {
                        // float alpha = *pRawValues++;
                        // TODO: handle alpha channel?
                    }

                    tmpData.emplace_back(red);
                    tmpData.emplace_back(green);
                    tmpData.emplace_back(blue);
                }
            }
            std::memcpy(&tmpData[0], &tex.data, sizeof(float));
            pMaterial->albedoMap = tex;
        }
    }
    else
    {
        // no UVs, create dummy pMaterial
        std::cout << "\tNo UVs detected, creating dummy material" << std::endl;
        // check for alpha channel value
        float alpha = pNukeMaterial->at(0, 0, Chan_Alpha);

        if (red == 0.0f && green == 0.0f && blue == 0.0f && alpha == 0.0f)
        {
            // there's no color, for the moment set it to some default...
            pMaterial->color = Mirage::Vec3(0.4f, 0.6f, 0.0f);
        }
        else
        {
            pMaterial->color = Mirage::Vec3(red, green, blue);
        }
    }
}


bool MirageRender::buildScene()
{
    std::cout << "Start: Building scene" << std::endl;

    // translate camera
    CameraOp* pCam = render_camera(0); // replace with getInputCameraOpForSampleAndView
    translateCamera(pCam);

    // build Nuke scene
    if (op_cast<GeoOp*>(input(1)) == nullptr)
    {
        std::cout << "Scene input is nullptr" << std::endl;
        return false;
    }
    Scene scene;
    GeoOp* pGeo = getInputGeoOpForSample(0);
    pGeo->build_scene(scene);

    // translate lights
    std::vector<LightContext*>::iterator itLight = scene.lights.begin();
    for (; itLight != scene.lights.end(); ++itLight)
    {
        LightContext* pLightC = *itLight;
        LightOp* pLight = pLightC->light();

        translateLight(pLight);
    }

    // build geometry list
    GeometryList geoList;
    pGeo->get_geometry(scene, geoList);

    // stop iterating if scene has no geometry
    if (geoList.size() == 0)
    {
        std::cout << "Scene has no geometry. Building scene stopped." << std::endl;
        m_scene.Clear(); // make sure to clear scene content
        return false;
    }

    // translate geometry
    for (size_t objIndex = 0; objIndex < geoList.size(); objIndex++)
    {
        GeoInfo& srcGeoObject = geoList.object(objIndex);
        translateGeo(srcGeoObject);
    }

    // stats
    std::cout << "Scene stats:" << std::endl;
    std::cout << "\t-> Primitives : " << m_scene.primitives.size() << std::endl;
    std::cout << "\t-> Meshes : " << m_scene.meshes.size() << std::endl;

    return true;
}