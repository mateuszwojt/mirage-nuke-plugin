# Nuke Path Tracer

Simple path tracer renderer for Nuke.

## Features

- Custom `DisneyShader` node

## Build instructions

This project includes CMake files for building the plugin on multiple platforms.

CentOS 7.6 and macOS 10.15 build are confirmed and working. Building this plugin on Windows may be possible, if you meet the requirements mentioned below.

Build requirements:

- CMake 3.9
- Nuke 12.1 (Nuke 11 and 10 untested, but should work)
- OpenMP (for multithreading)
- GCC 4.8.6 (Linux) / clang (macOS) / MSVC 14.0 - Visual Studio 2015 U3 (Windows)

1. Create `build` directory:

```
mkdir build && cd build
```

2. Run `cmake` in Release mode:

```
cmake -DCMAKE_BUILD_TYPE=Release ..
```

ATTENTION: building this plugin in Debug mode may result in Nuke crashing when nothing's connected to the renderer node.

3. Run `make`:

```
make && make install
```

4. CMake script will automatically create `init.py` script with plugin load procedure and copy it into `~\.nuke` directory.

5. Run Nuke and try creating `MirageRender` node.


## TODO

- [ ] Texture mapping
- [x] Thin lens approximation
- [ ] Nuke 3D primitives loading
- [ ] HDR probe loading
- [ ] Motion Blur

## Known limitations

- only triangle-based geometry renders correctly
- no GPU support at the moment

## Screenshots

![Buddha test render](./screenshots/budda_test_render.png)
---
![Buddha DOF](./screenshots/budda_dof_test_render.png)

## References

- Core path tracing code is based on Miles Macklins' amazing Tinsel renderer (https://github.com/mmacklin/tinsel)
- Various code fragments are based on work done by my dear friend (salut!) Nicolas Dumay (https://github.com/3djoser/NtoA) and Foundry's most briliant mind (now Weta's), Peter Pearson (https://github.com/ppearson)
- Hendrik Proosa's work and knowledge has also been a great resource (https://gitlab.com/hendrikproosa)