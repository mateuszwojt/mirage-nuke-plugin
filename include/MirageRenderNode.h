#ifndef MIRAGE_RENDER_NODE_H
#define MIRAGE_RENDER_NODE_H

#include <climits>

#include "DDImage/Iop.h"
#include "DDImage/Knobs.h"
#include "DDImage/Row.h"
#include "DDImage/CameraOp.h"
#include "DDImage/LightOp.h"
#include "DDImage/Render.h"

#include <core/Scene.h>
#include <core/Renderer.h>
#include <utils/Util.h>

#include "DisneyShaderNode.h"

#if _WIN32
#include <cuda_runtime.h>
#endif

static const char* const CLASS = "MirageRender";
static const char* const HELP = "Mirage Path Tracer CPU/GPU renderer";

using namespace DD::Image;

class MirageRender : public DD::Image::Render
{
public:
    MirageRender(Node* node);
    ~MirageRender() {};

    // scene building
    bool buildScene();
    void translateCamera(CameraOp* pCamera);
    void translateLight(LightOp* pLight);
    void translateGeo(GeoInfo& geoInfo);
    void translateMaterial(Mirage::Material* pMaterial, Iop* pNukeMaterial, bool haveUVs);

    // Renderer methods
    void preRender();
    void initRender();
    void render();
    void postRender();

    GeoOp* getInputGeoOpForSample(uint32_t sample);
    CameraOp* getInputCameraOpForSampleAndView(uint32_t sample, int32_t view);

    // Nuke methods
    int minimum_inputs() const { return 3; }
    int maximum_inputs() const { return 3; }
    bool test_input(int input, Op* op) const;
    Op* default_input(int input) const;
    const char* input_label(int input, char* buffer) const;
    int split_input(int input) const;
    const OutputContext& inputContext(int input, int offset, OutputContext& context) const;

    Op::HandlesMode doAnyHandles(DD::Image::ViewerContext*);
    void build_handles(DD::Image::ViewerContext*);

    void knobs(Knob_Callback);
    int knob_changed(Knob* k);
    void append(Hash& hash);
    void _validate(bool for_real);
    void _request(int x, int y, int r, int t, ChannelMask output_channels, int count);
    void _close();
    void engine(int y, int x, int r, ChannelMask channels, Row& row);

    CameraOp* render_camera(int sample) { return getInputCameraOpForSampleAndView(sample, 0/*view*/); }
    GeoOp* render_geo(int sample) { return getInputGeoOpForSample(sample); }
    Matrix4 camera_matrix(int sample);
    Matrix4 projection_matrix(int sample);
    double shutter() const { return m_renderOptions.maxSamples; }  // TODO: add shutter to camera/render options
    double offset() const { return m_renderOptions.maxSamples; }  // TODO: add offset calculation to render options
    uint32_t samples() const { return m_renderOptions.maxSamples; }
    bool generate_render_primitives();
    void initialize();

    static const Description description;
    const char* Class() const { return description.name; }
	const char* node_help() const { return HELP; }
    

private:
// Path Tracer stuff
    Mirage::Scene                   m_scene;
    Mirage::Options                 m_renderOptions;
    Mirage::Renderer*               m_pRenderer;
    Mirage::Camera                  m_Camera;
    Mirage::Color*                  m_pPixels;
    Mirage::Color*                  m_pPixelsFiltered;

    int                             m_renderType;
    int                             m_filterType;
    int                             m_renderMode;
    float                           m_exposure;
    float                           m_limit;
    int                             m_numSamples;
    bool                            m_enableDOF;
    int                             m_nlmWidth;
    float                           m_nlmFalloff;

    bool                            m_needsRender;

// Nuke knobs
    Lock                            m_lock;

    Hash                            m_sceneHash;

    unsigned int                    m_imageWidth;
    unsigned int                    m_imageHeight;
};

#endif // MIRAGE_RENDER_NODE_H