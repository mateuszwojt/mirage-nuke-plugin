#include <shaders/Disney.h>

#include "DDImage/IllumShader.h"
#include "DDImage/Knob.h"
#include "DDImage/Knobs.h"
#include "DDImage/gl.h"

using namespace DD::Image;

class DisneyShader : public IllumShader
{
public:
    DisneyShader(Node* node);

    // Shader export
    void exportMaterial(Mirage::Material* material);

    // Nuke methods
    virtual void surface_shader(Vector3& P, Vector3& V, Vector3& N, const VertexContext&, Pixel& surface);
    bool shade_GL(ViewerContext*, GeoInfo&);
    bool set_texturemap(ViewerContext* ctx, bool gl);

    Op* default_input(int input) const;
    const char* input_label(int input, char* buffer) const;

    void _validate(bool);
    void _request(int x, int y, int r, int t, ChannelMask channels, int count);

    virtual void knobs(Knob_Callback);

    int minimum_inputs() const
    {
    return 5;
    }

    int maximum_inputs() const
    {
    return 5;
    }

    const char* Class() const { return "DisneyShader"; }
	const char* displayName() const { return "DisneyShader"; }
	const char* node_help() const { return "Disney BSDF implemented as Nuke shader node"; }
    static const Iop::Description description;

private:
    // Disney shader attributes
    Vector3                 m_emission;
    Vector3                 m_color;
    Vector3                 m_absorption;

    int                     m_bsdfType;

    float                   m_eta;
    float                   m_metallic;
    float                   m_subsurface;
    float                   m_specular;
    float                   m_roughness;
    float                   m_specularTint;
    float                   m_anisotropic;
    float                   m_sheen;
    float                   m_sheenTint;
    float                   m_clearcoat;
    float                   m_clearcoatGloss;
    float                   m_transmission;
};